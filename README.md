# Industrial Programming "Anzeige und Analyse von Sensordaten"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Recherche / Implementierung und Konfiguration 

Siehe [LaTeX-Protokoll](https://www.overleaf.com/read/mqmndggsmqxy).

## Ausführen des Programmes

### Voraussetzung

*Rust* lauffähig auf Raspberry Pi. Falls nicht siehe [hier](https://www.overleaf.com/read/mqmndggsmqxy).

### Programm builden

```bash
cargo build
```

Dadurch sollte eine neue ausführbare Datei `rust_sensor ` unter `target/debug` entstehen.

### Ausführung

```
./target/debug/rust_sensor
```

## Quellen

* [Rust Installation Raspberry Pi](https://www.overleaf.com/read/mqmndggsmqxy)
* [Rust Peripherie (GPIO und mehr) API](https://github.com/golemparts/rppal)
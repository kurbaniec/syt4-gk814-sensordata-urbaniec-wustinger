extern crate lettre;
extern crate lettre_email;
extern crate pnet;
#[macro_use]
extern crate influx_db_client;

// Command
use std::process::Command;

// use pnet::datalink;

//Main
use lettre::smtp::authentication::{Credentials, Mechanism};
use lettre::{SendableEmail, Envelope, EmailAddress, Transport, SmtpClient};
use lettre::smtp::extension::ClientId;
use lettre::smtp::ConnectionReuseParameters;
use lettre_email::{Email, mime::TEXT_PLAIN};
use std::path::Path;
// Threads
use std::thread;
use std::time::Duration;
// Influx
use influx_db_client::{Client as InfClient, Point, Points, Value as IValue, Precision};
// GPIO
use rppal::gpio::Gpio;
use rppal::system::DeviceInfo;
use std::error::Error;

use rppal::i2c::I2c;

// GPIO BCM 15, BOARD 10
const GPIO_BUTTON: u8 = 15;

fn main() -> Result<(), Box<dyn Error>> {
    println!("Hello, world!");
    println!("I´m a {}", DeviceInfo::new()?.model());
    let test = temp_measurement();
    println!("{}", test);

    let mut listening = true;

    // Create and run thread
    // Internal light sensor
    thread::spawn(move || {
        while listening {
            // Get data via temp_measurement() and save to influx
            influx("Temp", "Value", &temp_measurement());
            thread::sleep(Duration::from_millis(1000));
        }
    });

    // I2C TSL2561 Lux Sensor - returns brightness in lux
    // Gets data via python script
    thread::spawn(move || {
        while listening {
            // Get data via light_measurement() and save to influx
            influx_int("Light", "Value", light_measueremnt());
            thread::sleep(Duration::from_millis(1000));
        }
    });


    // Define pin
    let mut pin = Gpio::new()?.get(GPIO_BUTTON)?.into_input();
    thread::spawn(move || {
        let mut counter = 0;        // Used for long button press notification
        let mut avg_counter = 0;    // Used for counting every cycle (~60s)
        let mut count = 0;          // Used for counting button presses in a cycle
        while listening {
            let read = pin.read();
            let mut store = 0;      // Value that is stored in the database
            // Read level
            // If Button is pressed, change store variable value
            // Increment some counters
            if read == rppal::gpio::Level::High {
                store = 1;
                counter += 1;
                avg_counter += 1;
                count += 1;
                println!("AVG: {} --", count);
            }
            // Just increment normal counter
            else {
                avg_counter += 1;
                counter = 0;
            }
            // If the button is pressed for around 10 second, send a warning email
            if counter == 10 {
                println!("Sending Button Notification");
                let msg = "Achtung!\nLange Betätigung des Tasters.".to_string();
                mail(msg);
                counter = 0;
            }
            // If a cycle is done, send the average viae mail
            if avg_counter == 60 {
                println!("Sending Button Average");
                avg_counter = 0;
                let msg = format!("Taster Durchschnitt\n{}/60", count);
                mail(msg);
                count = 0;
            }
            influx_int("Button", "Pressed", store);
            thread::sleep(Duration::from_millis(1000));
        }
    });
    // thread::sleep(Duration::from_millis(10000));
    // listening = false;

    while listening {

    }
    Ok(())
}

fn mail(msg: String) {
    // let mut ipstr = String::from("Hello!");
    /**for iface in datalink::interfaces() {
        ipstr.push_str(&("\n".to_owned() + &iface.name + "\n"));
        for adr in &iface.ips {
            ipstr.push_str(&adr.to_string());
            ipstr.push_str("\n");
        }
        ipstr.push_str("---");
    }*/
    //  println!("{}", ipstr);
    let email = SendableEmail::new(
        Envelope::new(
            Some(EmailAddress::new("syt.dummy@gmail.com".to_string()).unwrap()),
            vec![EmailAddress::new("syt.dummy@gmail.com".to_string()).unwrap()],
        ).unwrap(),
        "id".to_string(),
        msg.as_bytes().to_vec(),
    );

    let mut mailer = SmtpClient::new_simple("smtp.gmail.com").unwrap()
        // Set the name sent during EHLO/HELO, default is `localhost`
        .hello_name(ClientId::Domain("gmail.com".to_string()))
        // Add credentials for authentication
        .credentials(Credentials::new("syt.dummy".to_string(), "Dummy2018".to_string()))
        // Enable SMTPUTF8 if the server supports it
        .smtp_utf8(true)
        // Configure expected authentication mechanism
        .authentication_mechanism(Mechanism::Plain)
        // Enable connection reuse
        .connection_reuse(ConnectionReuseParameters::ReuseUnlimited).transport();

    let result = mailer.send(email);
    println!("{:?}", result);
    mailer.close();
}



fn influx(msm: &str, field: &str, val: &str) {
    // default with "http://127.0.0.1:8086", db with "test"
    let client = InfClient::default().set_authentication("root", "root");
    /**
    let mut point = point!("test1");
    point
        .add_field("foo", IValue::String("bar".to_string()))
        .add_field("integer", IValue::Integer(11))
        .add_field("float", IValue::Float(22.3))
        .add_field("'boolean'", IValue::Boolean(false));
    */
    /**
    let point1 = Point::new("test1")
        .add_tag("tags", IValue::String(String::from("\\\"fda")))
        .add_tag("number", IValue::Integer(12))
        .add_tag("float", IValue::Float(12.6))
        .add_field("fd", IValue::String("'3'".to_string()))
        .add_field("quto", IValue::String("\\\"fda".to_string()))
        .add_field("quto1", IValue::String("\"fda".to_string()))
        .to_owned();

     let points = points!(point1, point);

    // if Precision is None, the default is second
    // Multiple write
    let _ = client.write_points(points, Some(Precision::Seconds), None).unwrap();

    // query, it's type is Option<Vec<Node>>
    let res = client.query("select * from test1", None).unwrap();
    println!("{:?}", res.unwrap()[0].series);
        */
    let point = Point::new(&msm)
        .add_field(&field, IValue::String(String::from(val)))
        .to_owned();

    let _ = client.write_point(point, Some(Precision::Seconds), None).unwrap();

}

fn influx_int(msm: &str, field: &str, val: i64) {
    // default with "http://127.0.0.1:8086", db with "test"
    let client = InfClient::default().set_authentication("root", "root");
    let point = Point::new(&msm)
        .add_field(&field, IValue::Integer(val))
        .to_owned();
    let _ = client.write_point(point, Some(Precision::Seconds), None).unwrap();
}


fn temp_measurement() -> String {
    // Create and execute command
    let output = Command::new("/opt/vc/bin/vcgencmd")
        .arg("measure_temp")
        .output()
        .expect("Failed");
    let output_str: &str;
    let output_raw;
    // Generate string from output
    unsafe {
        output_raw = String::from_utf8_unchecked(output.stdout);
    }
    // Remoe "Temp=" from output string
    output_str = &output_raw[5..];
    return output_str.to_string();
}

fn light_measueremnt() -> i64 {
    // Create and execute command
    let output = Command::new("python")
        .arg("/home/pi/remote/python_sensor/Python/light_sensor.py")
        .output()
        .expect("Failed");
    let output_str: &str;
    let output_raw;
    // Generate string from output
    unsafe {
        output_raw = String::from_utf8_unchecked(output.stdout);
    }
    output_str = output_raw.trim();
    println!("{}", output_str);
    //let lux: i64 = output_raw.to_string().parse::<i64>().unwrap();
    //let test: i64 = ("123".to_string()).parse::<i64>().unwrap();
    //println!("baum");
    let lux: i64 = output_str.parse::<i64>().unwrap();
    return lux
}
